# we want to match the word : BREAD
import IO

hints = "tomato,cup cake, black berries, onion"
puts "hints: #{hints}"

guess = gets "Guess the word: "
# guess
guess = guess 
  |> String.trim

case guess do
  "bread" ->
    puts "You win this round!"

  _ ->
    puts " Sorry but you have lost"
end
